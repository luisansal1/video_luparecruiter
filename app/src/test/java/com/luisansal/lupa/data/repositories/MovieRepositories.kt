package com.luisansal.lupa.data.repositories

import com.luisansal.lupa.core.data.Result
import com.luisansal.lupa.core.utils.EMPTY
import com.luisansal.lupa.data.repository.MovieRepository
import com.luisansal.lupa.domain.entities.MovieEntity
import com.luisansal.lupa.domain.entities.VideoEntity
import com.luisansal.lupa.domain.usecases.MovieUsecase
import com.luisansal.lupa.utils.VideoType
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test

class MovieRepositories {

    private val movieRepository = mockk<MovieRepository>(relaxed = true)

    lateinit var movieUsecase: MovieUsecase

    @Before
    fun setUp() {
        movieUsecase = MovieUsecase(movieRepository)
    }

    private val movies = Result.Success(
        listOf(
            MovieEntity(
                id = 1,
                backgroundPath = EMPTY,
                posterPath = EMPTY,
                posterDetailPath = EMPTY,
                stars = 2.0,
                title = "movie 1",
                overview = "overview movie 1"
            ),
            MovieEntity(
                id = 2,
                backgroundPath = EMPTY,
                posterPath = EMPTY,
                posterDetailPath = EMPTY,
                stars = 5.0,
                title = "movie 2",
                overview = "overview movie 2"
            )
        )
    )

    private val videos = Result.Success(
        listOf(
            VideoEntity(
                id = "1",
                site = "site 1",
                type = VideoType.Trailer._name,
                key = "44ssfasdf"
            ),
            VideoEntity(
                id = "2",
                site = "site 2",
                type = VideoType.BehindTheScenes._name,
                key = "t572123"
            )
        )
    )

    @Test
    fun `get Upcoming movies`() = runBlocking {
        coEvery { movieRepository.getUpcoming() } returns movies

        val result = movieUsecase.getUpcoming()

        coVerify(exactly = 1) {
            movieRepository.getUpcoming()
        }

        assert(result == movies)
    }

    @Test
    fun `get top rated movies`() = runBlocking {
        coEvery { movieRepository.getTopRated() } returns movies

        val result = movieUsecase.getTopRated()

        coVerify(exactly = 1) {
            movieRepository.getTopRated()
        }

        assert(result == movies)
    }

    @Test
    fun `get a video`() = runBlocking {
        coEvery { movieRepository.getVideo(any()) } returns videos

        val result = movieUsecase.getTrailer(1)

        coVerify(exactly = 1) {
            movieRepository.getVideo(any())
        }

        when (result) {
            is Result.Success -> {
                assert(result.data == videos.data?.get(0))
            }
            is Result.Error -> {

            }
            else -> Unit
        }
    }

    @After
    fun tearDown() {
        unmockkAll()
    }
}