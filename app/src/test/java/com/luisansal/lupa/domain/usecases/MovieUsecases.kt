package com.luisansal.lupa.domain.usecases

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.luisansal.lupa.core.data.Result
import com.luisansal.lupa.core.utils.EMPTY
import com.luisansal.lupa.domain.entities.MovieEntity
import com.luisansal.lupa.domain.entities.VideoEntity
import com.luisansal.lupa.features.adapters.ScreenMovieAdapter
import com.luisansal.lupa.features.home.HomeViewModel
import com.luisansal.lupa.utils.VideoType
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class MovieUsecases {
    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    private val movieUsecase = mockk<MovieUsecase>(relaxed = true)

    lateinit var homeViewModel: HomeViewModel

    private val adapter = mockk<ScreenMovieAdapter>(relaxed = true)

    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        homeViewModel = HomeViewModel(movieUsecase)
    }

    private val movies = Result.Success(
        listOf(
            MovieEntity(
                id = 1,
                backgroundPath = EMPTY,
                posterPath = EMPTY,
                posterDetailPath = EMPTY,
                stars = 2.0,
                title = "movie 1",
                overview = "overview movie 1"
            ),
            MovieEntity(
                id = 2,
                backgroundPath = EMPTY,
                posterPath = EMPTY,
                posterDetailPath = EMPTY,
                stars = 5.0,
                title = "movie 2",
                overview = "overview movie 2"
            )
        )
    )

    private val videos = Result.Success(
        listOf(
            VideoEntity(
                id = "1",
                site = "site 1",
                type = VideoType.Trailer._name,
                key = "44ssfasdf"
            ),
            VideoEntity(
                id = "2",
                site = "site 2",
                type = VideoType.BehindTheScenes._name,
                key = "t572123"
            )
        )
    )

    @Test
    fun `when home is initialize`() = runBlocking {
        coEvery { movieUsecase.getUpcoming() } returns movies
        coEvery { movieUsecase.getTopRated() } returns movies
        coEvery { movieUsecase.getRecommendsToYou(any(), any()) } returns movies

        coVerify(exactly = 1) {
            movieUsecase.getUpcoming()
            movieUsecase.getTopRated()
            movieUsecase.getRecommendsToYou(any(), any())
        }
    }

    @Test
    fun `when user click on language`() = runBlocking {
        coEvery { movieUsecase.getRecommendsToYou(any(), any()) } returns movies

        homeViewModel.onClickLanguage()

        coVerify {
            movieUsecase.getRecommendsToYou(any(), any())
        }
    }

    @Test
    fun `when user click on release on 1993`() = runBlocking {
        coEvery { movieUsecase.getRecommendsToYou(any(), any()) } returns movies

        homeViewModel.onClickLanguage()

        coVerify {
            movieUsecase.getRecommendsToYou(any(), any())
        }
    }


    @After
    fun tearDown() {
        unmockkAll()
        Dispatchers.resetMain()
    }
}