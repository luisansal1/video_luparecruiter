package com.luisansal.lupa.utils

enum class MovieType(val _name: String) {
    Upcoming("Upcoming"),
    TopRated("TopRated")
}

enum class MovieImageType(val path: String) {
    Poster("w200/"),
    Detail("w500/")
}

enum class VideoType(val _name: String) {
    Trailer("Trailer"),
    Clip("clip"),
    BehindTheScenes("Behind the Scenes")
}