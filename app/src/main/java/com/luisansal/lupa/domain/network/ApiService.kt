package com.luisansal.lupa.domain.network

import com.luisansal.lupa.core.BuildConfig
import com.luisansal.lupa.data.network.response.TopRatedResponse
import com.luisansal.lupa.data.network.response.UpcomingResponse
import com.luisansal.lupa.data.network.response.VideoResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    companion object {
        const val BASE_URL: String = BuildConfig.BASE_HOST
    }

    @GET("movie/upcoming")
    suspend fun getUpcomming(): Response<UpcomingResponse>

    @GET("movie/top_rated")
    suspend fun getTopRated(): Response<TopRatedResponse>

    @GET("movie/{videoId}/videos")
    suspend fun getMovie(@Path("videoId") videoId: Long): Response<VideoResponse>
}