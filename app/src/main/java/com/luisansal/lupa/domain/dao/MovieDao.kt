package com.luisansal.lupa.domain.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.luisansal.lupa.domain.entities.MovieEntity

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(movie: MovieEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(movies: List<MovieEntity>)

    @Query("DELETE FROM tblmovie")
    fun deleteAll()

    @Query("select count(*) FROM tblmovie")
    fun count(): Long

    @Query("SELECT * from tblmovie ORDER BY title ASC")
    fun findAll(): List<MovieEntity>

    @Query("SELECT * from tblmovie where type = :type ORDER BY title ASC")
    fun findByType(type: String): List<MovieEntity>

    @Query("SELECT * from tblmovie where id = :id")
    fun findOneById(id: Long): MovieEntity?

    @Query("DELETE FROM tblmovie where id = :id")
    fun deleteUser(id: Long): Int

    @Query("DELETE FROM tblmovie")
    fun deleteAllUser(): Int

}
