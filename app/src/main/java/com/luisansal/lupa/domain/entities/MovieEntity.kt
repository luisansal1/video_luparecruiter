package com.luisansal.lupa.domain.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.luisansal.lupa.core.utils.EMPTY
import com.luisansal.lupa.utils.MovieType
import java.io.Serializable

@Entity(tableName = "tblmovie")
data class MovieEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val id: Long,
    val backgroundPath: String,
    val posterPath: String,
    val posterDetailPath: String,
    val language: String = "en",
    val stars: Double = 0.0,
    val yearRelease: String = EMPTY,
    val hasVideo: Boolean = false,
    val hasTrailer: Boolean = false,
    val title: String,
    val overview: String,
    var type: String = EMPTY
) : Serializable