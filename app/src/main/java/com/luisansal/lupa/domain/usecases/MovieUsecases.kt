package com.luisansal.lupa.domain.usecases

import com.luisansal.lupa.core.data.Result
import com.luisansal.lupa.data.repository.MovieRepository
import com.luisansal.lupa.domain.entities.MovieEntity
import com.luisansal.lupa.domain.entities.VideoEntity
import com.luisansal.lupa.features.home.RecommendType
import com.luisansal.lupa.utils.VideoType
import javax.inject.Inject

class MovieUsecase @Inject constructor(private val movieRepository: MovieRepository) {
    suspend fun getUpcoming(): Result<List<MovieEntity>> = movieRepository.getUpcoming()
    suspend fun getTopRated(): Result<List<MovieEntity>> = movieRepository.getTopRated()
    suspend fun getTrailer(movieId: Long): Result<VideoEntity> = when (val result = movieRepository.getVideo(movieId)) {
        is Result.Success -> {
            val data = result.data?.find { it.type == VideoType.Trailer._name }
            Result.Success(data)
        }
        is Result.Error -> {
            Result.Error(result.exception)
        }
        else -> Result.Success(null)
    }

    suspend fun getRecommendsToYou(type: RecommendType, text: String): Result<List<MovieEntity>> {
        return when (val result = movieRepository.getTopRated()) {
            is Result.Success -> {
                var data = emptyList<MovieEntity>()
                when (type) {
                    RecommendType.Spanish -> {
                        result.data?.let {
                            data = it.filter { it.language == text }.take(6)
                        }
                    }
                    RecommendType.Release1993 -> {
                        result.data?.let {
                            data = it.filter { it.yearRelease.toInt() == text.toInt() }.take(6)
                        }
                    }
                }
                Result.Success(data)
            }
            is Result.Error -> {
                Result.Error(result.exception)
            }
            else -> Result.Success(emptyList())
        }
    }
}