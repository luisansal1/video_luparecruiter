package com.luisansal.lupa.domain.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.luisansal.lupa.domain.entities.VideoEntity

@Dao
interface VideoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(video: VideoEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(videos: List<VideoEntity>)

    @Query("DELETE FROM tblvideo")
    fun deleteAll()

    @Query("select count(*) FROM tblvideo")
    fun count(): Long

    @Query("select * FROM tblvideo")
    fun findAll(): List<VideoEntity>

    @Query("SELECT * from tblvideo where type = :type")
    fun findByType(type: String): List<VideoEntity>

    @Query("SELECT * from tblvideo where id = :id")
    fun findOneById(id: Long): VideoEntity?

    @Query("DELETE FROM tblvideo where id = :id")
    fun deleteUser(id: Long): Int

    @Query("DELETE FROM tblvideo")
    fun deleteAllUser(): Int

}
