package com.luisansal.lupa.di

import android.content.Context
import com.luisansal.lupa.core.data.network.RetrofitConfig
import com.luisansal.lupa.data.BaseRoomDatabase
import com.luisansal.lupa.domain.dao.MovieDao
import com.luisansal.lupa.domain.dao.VideoDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Inject
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule @Inject constructor() {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext appContext: Context): Context {
        return appContext
    }
}

@Module
@InstallIn(SingletonComponent::class)
class ApiModule @Inject constructor() {
    @Singleton
    @Provides
    fun getApi() =
        RetrofitConfig(com.luisansal.lupa.domain.network.ApiService.BASE_URL).creteService(com.luisansal.lupa.domain.network.ApiService::class.java)
}

@Module
@InstallIn(SingletonComponent::class)
class DataBaseModule @Inject constructor() {
    @Singleton
    @Provides
    fun provideRoomDatabase(@ApplicationContext appContext: Context): BaseRoomDatabase {
        return BaseRoomDatabase.getDatabase(appContext)!!
    }

    @Singleton
    @Provides
    fun provideMovie(database: BaseRoomDatabase): MovieDao {
        return database.movieDao()
    }

    @Singleton
    @Provides
    fun provideVideo(database: BaseRoomDatabase): VideoDao {
        return database.videoDao()
    }
}

