package com.luisansal.lupa.features.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.luisansal.lupa.core.base.BaseViewModel
import com.luisansal.lupa.core.data.*
import com.luisansal.lupa.domain.entities.MovieEntity
import com.luisansal.lupa.domain.usecases.MovieUsecase
import com.luisansal.lupa.features.adapters.ScreenMovieAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val movieUsecase: MovieUsecase) : BaseViewModel() {
    companion object {
        const val language = "es"
        const val year = "1993"
    }

    val upcomingAdapter by lazy {
        ScreenMovieAdapter { movieEntity ->
            _imageClicked.value = movieEntity
            _imageClicked.value = null
        }
    }

    val topRatedAdapter by lazy {
        ScreenMovieAdapter { movieEntity ->
            _imageClicked.value = movieEntity
            _imageClicked.value = null
        }
    }

    val recommendsAdapter by lazy {
        ScreenMovieAdapter { movieEntity ->
            _imageClicked.value = movieEntity
            _imageClicked.value = null
        }
    }

    private val _imageClicked = MutableLiveData<MovieEntity?>()
    val imageClicked: LiveData<MovieEntity?> get() = _imageClicked

    private val _selectedLanguage = MutableLiveData(true)
    val selectedLanguage: LiveData<Boolean> get() = _selectedLanguage

    init {
        load()
    }

    private fun load() {
        viewModelScope.launch {
            val deferrers = listOf(
                async { getUpcoming() },
                async { getTopRated() },
                async { getRecommends(RecommendType.Spanish, language) }
            )
            deferrers.awaitAll()
        }
    }

    private suspend fun getUpcoming() {
        when (val result = movieUsecase.getUpcoming()) {
            is Result.Success -> {
                upcomingAdapter.submitList(result.data)
            }
            is Result.Error -> {
                Log.d("error", "${result.exception}")
            }
            else -> {}
        }
    }

    private suspend fun getTopRated() {
        when (val result = movieUsecase.getTopRated()) {
            is Result.Success -> {
                topRatedAdapter.submitList(result.data)
            }
            is Result.Error -> {
                Log.d("error", "${result.exception}")
            }
            else -> {}
        }
    }

    private suspend fun getRecommends(type: RecommendType, text: String) {
        when (val result = movieUsecase.getRecommendsToYou(type, text)) {
            is Result.Success -> {
                recommendsAdapter.submitList(result.data)
            }
            is Result.Error -> {
                Log.d("error", "${result.exception}")
            }
            else -> {}
        }
    }

    fun onClickLanguage() {
        _selectedLanguage.value = true
        viewModelScope.launch {
            getRecommends(RecommendType.Spanish, language)
        }
    }

    fun onClickYear() {
        _selectedLanguage.value = false
        viewModelScope.launch {
            getRecommends(RecommendType.Release1993, year)
        }
    }

}