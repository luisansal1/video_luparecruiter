package com.luisansal.lupa.features.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.luisansal.lupa.R
import com.luisansal.lupa.core.base.BaseBindingFragment
import com.luisansal.lupa.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseBindingFragment() {
    val binding by lazy {
        FragmentHomeBinding.inflate(layoutInflater).apply {
            lifecycleOwner = this@HomeFragment
        }
    }

    override fun getViewResource() = binding.root
    private val viewModel by viewModels<HomeViewModel>()
    private val navController by lazy {
        Navigation.findNavController(this.requireActivity(), R.id.nav_home_fragment)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewModel.imageClicked.observe(viewLifecycleOwner) { movieEntity ->
            movieEntity ?: return@observe
            navController.navigate(HomeFragmentDirections.actionHomeToDetail(movieEntity))
        }
    }
}