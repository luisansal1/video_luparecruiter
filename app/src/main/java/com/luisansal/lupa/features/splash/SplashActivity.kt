package com.luisansal.lupa.features.splash

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.luisansal.lupa.core.base.BaseBindingActivity
import com.luisansal.lupa.databinding.ActivitySplashBinding
import com.luisansal.lupa.features.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : BaseBindingActivity() {

    private val binding by lazy {
        ActivitySplashBinding.inflate(layoutInflater).apply {
            lifecycleOwner = this@SplashActivity
        }
    }

    override fun getViewResource() = binding.root

    private val viewModel by viewModels<SplashViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewModel.isLogoClicked.observe(this) {
            if (it)
                startActivity(HomeActivity.newIntent(this))
        }
    }
}