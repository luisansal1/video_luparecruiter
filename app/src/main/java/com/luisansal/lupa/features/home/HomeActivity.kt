package com.luisansal.lupa.features.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.luisansal.lupa.core.base.BaseBindingActivity
import com.luisansal.lupa.databinding.ActivityHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : BaseBindingActivity() {
    companion object{
        fun newIntent(context: Context) = Intent(context, HomeActivity::class.java)
    }

    private val binding by lazy {
        ActivityHomeBinding.inflate(layoutInflater).apply {
            lifecycleOwner = this@HomeActivity
        }
    }
    override fun getViewResource() = binding.root

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}