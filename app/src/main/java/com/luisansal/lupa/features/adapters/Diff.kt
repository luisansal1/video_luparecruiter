package com.luisansal.lupa.features.adapters

import androidx.recyclerview.widget.DiffUtil
import com.luisansal.lupa.domain.entities.MovieEntity

class MovieEntityDiff : DiffUtil.ItemCallback<MovieEntity>() {
    override fun areItemsTheSame(oldItem: MovieEntity, newItem: MovieEntity): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: MovieEntity, newItem: MovieEntity): Boolean {
        return oldItem == newItem
    }

}