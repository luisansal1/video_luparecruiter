package com.luisansal.lupa.features.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.luisansal.lupa.core.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor() : BaseViewModel() {
    private val _isLogoClicked = MutableLiveData<Boolean>(false)
    val isLogoClicked: LiveData<Boolean> get() = _isLogoClicked

    fun onClickLogo() {
        _isLogoClicked.value = true
    }
}