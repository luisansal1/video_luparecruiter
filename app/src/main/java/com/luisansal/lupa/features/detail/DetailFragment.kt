package com.luisansal.lupa.features.detail

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.luisansal.lupa.core.base.BaseBindingFragment
import com.luisansal.lupa.databinding.FragmentDetailMovieBinding
import com.luisansal.lupa.domain.usecases.MovieUsecase
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailFragment : BaseBindingFragment() {
    val binding by lazy {
        FragmentDetailMovieBinding.inflate(layoutInflater).apply {
            lifecycleOwner = this@DetailFragment
        }
    }

    override fun getViewResource() = binding.root

    private val args by navArgs<DetailFragmentArgs>()

    @Inject
    lateinit var movieUsecase: MovieUsecase

    private val viewModel by viewModels<DetailViewModel> { DetailViewModel.Factory(requireActivity(), args, movieUsecase) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewModel.onClickBack.observe(viewLifecycleOwner) {
            if (it)
                requireActivity().onBackPressed()
        }
    }
}