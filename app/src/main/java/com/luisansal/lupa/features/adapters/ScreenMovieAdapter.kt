package com.luisansal.lupa.features.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.luisansal.lupa.databinding.ItemMovieBinding
import com.luisansal.lupa.domain.entities.MovieEntity
import com.squareup.picasso.Picasso

class ScreenMovieAdapter(private val clickListener: (MovieEntity) -> Unit) :
    ListAdapter<MovieEntity, ScreenMovieAdapter.ViewHolder>(MovieEntityDiff()) {

    inner class ViewHolder(private val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: MovieEntity) {
            Picasso.Builder(binding.root.context).build()
                .load(item.posterPath)
                .into(binding.ivMovie)
            binding.root.setOnClickListener {
                clickListener(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

}