package com.luisansal.lupa.features.detail

import androidx.activity.ComponentActivity
import androidx.lifecycle.*
import com.luisansal.lupa.core.base.BaseViewModel
import com.luisansal.lupa.core.data.Result
import com.luisansal.lupa.core.utils.watchYoutubeVideo
import com.luisansal.lupa.domain.usecases.MovieUsecase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch

class DetailViewModel(
    private val context: ComponentActivity, private val args: DetailFragmentArgs,
    private val movieUsecase: MovieUsecase
) : BaseViewModel() {

    private val _onClickBack = MutableLiveData(false)
    val onClickBack: LiveData<Boolean> get() = _onClickBack

    val entity = args.movieEntity
    val stars: String = entity.stars.toString()

    fun onClickBack() {
        _onClickBack.value = true
    }

    fun watchTrailer() {
        viewModelScope.launch {
            when (val result = movieUsecase.getTrailer(args.movieEntity.id)) {
                is Result.Success -> {
                    result.data?.let {
                        watchYoutubeVideo(context, it.key)
                    }
                }
                is Result.Error -> {
                    errorDialog.postValue(result.exception)
                }
                else -> Unit
            }

        }

    }

    class Factory(
        private val context: ComponentActivity, private val args: DetailFragmentArgs,
        private val movieUsecase: MovieUsecase
    ) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return DetailViewModel(context, args, movieUsecase) as T
        }
    }
}


