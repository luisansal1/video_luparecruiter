package com.luisansal.lupa

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class LupaApp : Application() {

}