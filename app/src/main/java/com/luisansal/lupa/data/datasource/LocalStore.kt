package com.luisansal.lupa.data.datasource

import com.luisansal.lupa.domain.dao.MovieDao
import com.luisansal.lupa.domain.dao.VideoDao
import javax.inject.Inject

class LocalStore @Inject constructor(
    val movieDao: MovieDao,
    val videoDao: VideoDao
)