package com.luisansal.lupa.data.repository

import com.luisansal.lupa.core.data.Result
import com.luisansal.lupa.data.datasource.LocalStore
import com.luisansal.lupa.data.datasource.MovieCloudStore
import com.luisansal.lupa.domain.entities.MovieEntity
import com.luisansal.lupa.domain.entities.VideoEntity
import com.luisansal.lupa.utils.MovieType
import javax.inject.Inject

class MovieRepository @Inject constructor(private val movieCloudStore: MovieCloudStore, localStore: LocalStore) {
    private val movieDao = localStore.movieDao
    private val videoDao = localStore.videoDao
    suspend fun getUpcoming(): Result<List<MovieEntity>> = when (val result = movieCloudStore.getUpcoming()) {
        is Result.Success -> {
            result.data?.let {
                movieDao.saveAll(it.map { movieEntity ->
                    movieEntity.type = MovieType.Upcoming._name
                    movieEntity
                })
            }
            Result.Success(result.data)
        }
        is Result.Error -> {
            Result.Success(movieDao.findByType(MovieType.Upcoming._name))
        }
        else -> Result.Success(emptyList())
    }

    suspend fun getTopRated(): Result<List<MovieEntity>> = when (val result = movieCloudStore.getTopRated()) {
        is Result.Success -> {
            result.data?.let {
                movieDao.saveAll(it.map { movieEntity ->
                    movieEntity.type = MovieType.TopRated._name
                    movieEntity
                })
            }
            Result.Success(result.data)
        }
        is Result.Error -> {
            Result.Success(movieDao.findByType(MovieType.TopRated._name))
        }
        else -> Result.Success(emptyList())
    }

    suspend fun getVideo(movieId: Long): Result<List<VideoEntity>> = when (val result = movieCloudStore.getMovie(movieId)) {
        is Result.Success -> {
            result.data?.let {
                videoDao.saveAll(it)
            }
            Result.Success(result.data)
        }
        is Result.Error -> {
            Result.Success(videoDao.findAll())
        }
        else -> Result.Success(emptyList())
    }
}