package com.luisansal.lupa.data.network.response

import com.google.gson.annotations.SerializedName

data class UpcomingResponse(
    @SerializedName("results")
    val results: List<ObjMovie>
) {
    data class ObjMovie(
        @SerializedName("id")
        val id: Long,
        @SerializedName("backdrop_path")
        val backgroundPath: String,
        @SerializedName("poster_path")
        val posterPath: String,
        @SerializedName("original_language")
        val language: String,
        @SerializedName("release_date")
        val releaseDate: String,
        @SerializedName("video")
        val hasVideo: Boolean = false,
        @SerializedName("original_title")
        val title: String,
        @SerializedName("overview")
        val overview: String,
        @SerializedName("vote_average")
        val voteAverage: Double
    )
}