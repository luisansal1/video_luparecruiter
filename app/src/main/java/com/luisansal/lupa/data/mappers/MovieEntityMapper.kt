package com.luisansal.lupa.data.mappers

import com.luisansal.lupa.core.BuildConfig
import com.luisansal.lupa.data.network.response.TopRatedResponse
import com.luisansal.lupa.data.network.response.UpcomingResponse
import com.luisansal.lupa.data.network.response.VideoResponse
import com.luisansal.lupa.domain.entities.MovieEntity
import com.luisansal.lupa.domain.entities.VideoEntity
import com.luisansal.lupa.utils.MovieImageType

fun UpcomingResponse.toEntity() = this.results.map { upcomingMovie ->
    MovieEntity(
        id = upcomingMovie.id,
        backgroundPath = BuildConfig.IMAGE_HOST + MovieImageType.Detail.path + upcomingMovie.backgroundPath,
        posterPath = BuildConfig.IMAGE_HOST + MovieImageType.Poster.path + upcomingMovie.posterPath,
        posterDetailPath = BuildConfig.IMAGE_HOST + MovieImageType.Detail.path + upcomingMovie.posterPath,
        title = upcomingMovie.title,
        overview = upcomingMovie.overview,
        yearRelease = upcomingMovie.releaseDate.take(4),
        language = upcomingMovie.language,
        stars = upcomingMovie.voteAverage
    )
}


fun TopRatedResponse.toEntity() = this.results.map { topRatedMovie ->
    MovieEntity(
        id = topRatedMovie.id,
        backgroundPath = BuildConfig.IMAGE_HOST + MovieImageType.Detail.path + topRatedMovie.backgroundPath,
        posterPath = BuildConfig.IMAGE_HOST + MovieImageType.Poster.path + topRatedMovie.posterPath,
        posterDetailPath = BuildConfig.IMAGE_HOST + MovieImageType.Detail.path + topRatedMovie.posterPath,
        title = topRatedMovie.title,
        overview = topRatedMovie.overview,
        yearRelease = topRatedMovie.releaseDate.take(4),
        language = topRatedMovie.language,
        stars = topRatedMovie.voteAverage
    )
}

fun VideoResponse.toEntity() = this.results.map { video ->
    VideoEntity(
        id = video.id,
        key = video.key,
        site = video.site,
        type = video.type
    )
}
