package com.luisansal.lupa.data.datasource

import com.luisansal.lupa.core.data.Result
import com.luisansal.lupa.core.utils.apiService
import com.luisansal.lupa.data.mappers.toEntity
import com.luisansal.lupa.domain.entities.MovieEntity
import com.luisansal.lupa.domain.entities.VideoEntity
import com.luisansal.lupa.domain.network.ApiService
import javax.inject.Inject

class MovieCloudStore @Inject constructor(private val apiService: ApiService) {
    suspend fun getUpcoming(): Result<List<MovieEntity>> {
        return apiService {
            val response = apiService.getUpcomming()
            val body = response.body()
            result(response) {
                body?.toEntity()
            }
        }
    }

    suspend fun getTopRated(): Result<List<MovieEntity>> {
        return apiService {
            val response = apiService.getTopRated()
            val body = response.body()
            result(response) {
                body?.toEntity()
            }
        }
    }

    suspend fun getMovie(movieId : Long): Result<List<VideoEntity>> {
        return apiService {
            val response = apiService.getMovie(movieId)
            val body = response.body()
            result(response) {
                body?.toEntity()
            }
        }
    }
}