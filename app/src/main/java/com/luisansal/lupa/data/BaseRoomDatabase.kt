package com.luisansal.lupa.data

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.luisansal.lupa.domain.dao.MovieDao
import com.luisansal.lupa.domain.dao.VideoDao
import com.luisansal.lupa.domain.entities.MovieEntity
import com.luisansal.lupa.domain.entities.VideoEntity

@Database(entities = [MovieEntity::class,VideoEntity::class], version = 1)
abstract class BaseRoomDatabase : RoomDatabase() {

    var isTest = false
    abstract fun movieDao(): MovieDao
    abstract fun videoDao(): VideoDao

    private class PopulateDbAsync(baseRoomDatabase: BaseRoomDatabase) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {

            return null
        }
    }

    companion object {

        @Volatile
        private var INSTANCE: BaseRoomDatabase? = null

        fun getDatabase(context: Context): BaseRoomDatabase? {
            if (INSTANCE == null) {
                synchronized(BaseRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context, BaseRoomDatabase::class.java, "lupaDatabase")
                                .fallbackToDestructiveMigration()
                                .allowMainThreadQueries()
                                .addCallback(sRoomDatabaseCallback)
                                .build()
                    }
                }
            }
            return INSTANCE
        }

        private val sRoomDatabaseCallback = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                if (!INSTANCE?.isTest!!)
                    PopulateDbAsync(INSTANCE!!).execute()
            }
        }
    }
}