package com.luisansal.lupa.data.network.response

import com.google.gson.annotations.SerializedName

data class VideoResponse(
    @SerializedName("results")
    val results: List<ObjVideo>
) {
    data class ObjVideo(
        @SerializedName("id")
        val id: String,
        @SerializedName("site")
        val site: String,
        @SerializedName("type")
        val type: String,
        @SerializedName("key")
        val key: String
    )
}