package com.luisansal.lupa.core.domain.exceptions

class ConnectException(message: String = "Conexión interrumpida") : Exception(message)
class UnknownHostException(message: String = "No hay conexión a internet") : Exception(message)
class SocketTimeoutException(message: String = "No hay respuesta en la conexión") : Exception(message)
class UnauthorizedException(message: String = "Usuario no autorizado") : Exception(message)
class UnexpectedException(message: String? = "Excepción no esperada") : Exception(message)
class HttpException(message: String = "Error en la petición http") : Exception(message)
class NotFoundException(message: String = "Not Found"):Exception(message)
class ServiceErrorException(message: String = "Error en el servicio"):Exception(message)
class ErrorLogicServerException(message: String = "Error inesperado en el servicio"):Exception(message)
class RequestResouseForbiddenException(message: String = "Request Resouse Forbidden Exception") : Exception(message)