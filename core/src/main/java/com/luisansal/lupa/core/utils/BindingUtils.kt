package com.luisansal.lupa.core.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("app:loadImagePath")
fun ImageView.loadImagePath(path: String){
    Picasso.Builder(context).build()
        .load(path)
        .into(this)
}