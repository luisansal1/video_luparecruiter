package com.luisansal.lupa.core.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

abstract class BaseBindingActivity : AppCompatActivity() {

    protected abstract fun getViewResource(): View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getViewResource())
    }
}
